//
//  ViewController.swift
//  dzthird
//
//  Created by student4 on 11/30/19.
//  Copyright © 2019 student4. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    var aboutStudent = (name: "Артем", surname: "Назюта", patronymic: "Михайлович", age: 15)
    var students: [(name: String, surname: String, patronymic: String, age: Int)] = []
    var studentPosition: [String] = []
    var game:[String] = []
    
    func helloStudents() {
        students.insert(aboutStudent, at: 0)
        aboutStudent = ("Михаил","Михайлов","Михайлович",19)
        students.insert(aboutStudent, at: 1)
        aboutStudent = ("Артем","Артемов","Артемович",45)
        students.insert(aboutStudent, at: 2)
        aboutStudent = ("Петр","Петров","Петрович",11)
        students.insert(aboutStudent, at: 3)
        aboutStudent = ("Мага","Иртыв","Аль-Манаг",78)
        students.insert(aboutStudent, at: 4)
        aboutStudent = ("Илья","Козлов","Дмитриевич",14)
        students.insert(aboutStudent, at: 5)
    }
    
    func loveGame() {
        game.insert("футбол", at: 0)
        game.insert("маджонг", at: 1)
        game.insert("покер", at: 2)
        game.insert("волейбол", at: 3)
        
    }
    
    func position() {
        studentPosition.insert("Разработчик", at: 0)
        studentPosition.insert("Бизнес-аналитик", at: 1)
        studentPosition.insert("SEO-man", at: 2)
        studentPosition.insert("Backend", at: 3)
        studentPosition.insert("Frontend", at: 4)
        studentPosition.insert("Менеджер", at: 5)
        
    }
    
    @IBOutlet weak var FIO: UILabel!
    @IBOutlet weak var AgeCountry: UILabel!
    @IBOutlet weak var Position: UILabel!
    @IBOutlet weak var Skill: UILabel!
    @IBOutlet weak var HourRate: UILabel!
    @IBOutlet weak var WeekRate: UILabel!
    @IBOutlet weak var AboutStudent: UITextView!
    @IBAction func ChangeUser(_ sender: Any) {
        helloStudents()
        position()
        loveGame()
        var i = Int.random(in: 0...students.count-1)
        let j = Int.random(in: 0...game.count-1)
        self.FIO.text = "\(students[i].surname) \(students[i].name) \(students[i].patronymic)"
        self.AgeCountry.text = "\(students[i].age), Беларусь"
        self.AboutStudent.text = "\(students[i].surname)  \(students[i].name)  \(students[i].patronymic)  \(students[i].age)  лет\nСтрана: Беларусь\n Любит \(game[j])"
        i = Int.random(in: 0...studentPosition.count-1)
        self.Position.text = "\(studentPosition[i])"
        i = Int.random(in: 1...15)
        self.Skill.text = "Опыт \(i) лет"
        
    }
    @IBAction func Pay(_ sender: Any) {
        helloStudents()
        var i = Int.random(in: 100...500)
        self.WeekRate.text = "\(i) BYN"
        self.HourRate.text = "\(i/7/24) BYN"
        
    }
}

